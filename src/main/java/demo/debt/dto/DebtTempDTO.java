package demo.debt.dto;

import java.util.Date;

public class DebtTempDTO {
    private Integer stt;
    private String erpCode;
    private String partnerName;
    private Date dataDate;
    private Long amountADebtB;
    private Long amountBDebtA;
    private Boolean isOK;

    public DebtTempDTO() {
    }

    public Integer getStt() {
        return stt;
    }

    public void setStt(Integer stt) {
        this.stt = stt;
    }

    public String getErpCode() {
        return erpCode;
    }

    public void setErpCode(String erpCode) {
        this.erpCode = erpCode;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public Date getDataDate() {
        return dataDate;
    }

    public void setDataDate(Date dataDate) {
        this.dataDate = dataDate;
    }

    public Long getAmountADebtB() {
        return amountADebtB;
    }

    public void setAmountADebtB(Long amountADebtB) {
        this.amountADebtB = amountADebtB;
    }

    public Long getAmountBDebtA() {
        return amountBDebtA;
    }

    public void setAmountBDebtA(Long amountBDebtA) {
        this.amountBDebtA = amountBDebtA;
    }

    public Boolean getOK() {
        return isOK;
    }

    public void setOK(Boolean OK) {
        isOK = OK;
    }
}
