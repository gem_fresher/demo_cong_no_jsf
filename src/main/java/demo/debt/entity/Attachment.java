package demo.debt.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "BBDC_Dinh_Kem")
public class Attachment extends BaseEntity {
    @ManyToOne
    @JoinColumn(name = "BBDC_ID")
    private DebtReconciliation debtReconciliation;

    @Column private String code;
    @Column private String value;
    @Column private Date createDate;
    @Column private String createUser;

    public Attachment() {
    }

    public DebtReconciliation getDebtReconciliation() {
        return debtReconciliation;
    }

    public void setDebtReconciliation(DebtReconciliation debtReconciliation) {
        this.debtReconciliation = debtReconciliation;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }
}
