package demo.debt.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "bbdc_log")
public class Log extends BaseEntity {
    @Column(name = "object_id")
    private  long objectId;
    @Column(name = "action_user")
    private String actionUser;
    @Column(name = "old_status")
    private short oldStatus;
    @Column(name = "new_status")
    private short newStatus;
    @Column(name = "action_date")
    private Date actionDate;
    @Column(name = "note")
    private String note;

    public Log() {
    }

    public long getObjectId() {
        return objectId;
    }

    public void setObjectId(long objectId) {
        this.objectId = objectId;
    }

    public String getActionUser() {
        return actionUser;
    }

    public void setActionUser(String actionUser) {
        this.actionUser = actionUser;
    }

    public short getOldStatus() {
        return oldStatus;
    }

    public void setOldStatus(short oldStatus) {
        this.oldStatus = oldStatus;
    }

    public short getNewStatus() {
        return newStatus;
    }

    public void setNewStatus(short newStatus) {
        this.newStatus = newStatus;
    }

    public Date getActionDate() {
        return actionDate;
    }

    public void setActionDate(Date actionDate) {
        this.actionDate = actionDate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
