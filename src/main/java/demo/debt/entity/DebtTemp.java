package demo.debt.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "Du_Lieu_Cong_No_Temp")
public class DebtTemp extends BaseEntity {

    @Column(name = "DATA_DATE")
    private Date dataDate;

    @Column(name = "ERP_CODE")
    private String erpCode;

    @Column
    private String partnerName;

    @Column
    private Integer amount;

    @Column
    private Integer dataType;

    @Column
    private String uploadFileName;

    @Column
    private String createUser;

    @Column
    private Date createDate;

    @Column
    private Long uploadId;

    @Column
    private Integer isSum;

    public DebtTemp() {
    }

    public Date getDataDate() {
        return dataDate;
    }

    public void setDataDate(Date dataDate) {
        this.dataDate = dataDate;
    }

    public String getErpCode() {
        return erpCode;
    }

    public void setErpCode(String erpCode) {
        this.erpCode = erpCode;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getDataType() {
        return dataType;
    }

    public void setDataType(Integer dataType) {
        this.dataType = dataType;
    }

    public String getUploadFileName() {
        return uploadFileName;
    }

    public void setUploadFileName(String uploadFileName) {
        this.uploadFileName = uploadFileName;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getUploadId() {
        return uploadId;
    }

    public void setUploadId(Long uploadId) {
        this.uploadId = uploadId;
    }

    public Integer getIsSum() {
        return isSum;
    }

    public void setIsSum(Integer isSum) {
        this.isSum = isSum;
    }
}
