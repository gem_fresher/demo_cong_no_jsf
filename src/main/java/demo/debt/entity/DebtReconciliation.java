package demo.debt.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "Bien_Ban_Doi_Chieu")
public class DebtReconciliation extends BaseEntity {
    @Column private Date dataDate;
    @Column private String erpCode;
    @Column private String partnerName;
    @Column private String representative;
    @Column private String amount;
    @Column private Integer dataType;
    @Column private String userUpload;
    @Column private Integer status;
    @Column private Integer flowSign;
    @Column private Integer amountB;
    @Column private Integer confirm;
    @Column private String note;
    @Column private Integer isSentMail;

    public DebtReconciliation() {
    }

    public Date getDataDate() {
        return dataDate;
    }

    public void setDataDate(Date dataDate) {
        this.dataDate = dataDate;
    }

    public String getErpCode() {
        return erpCode;
    }

    public void setErpCode(String erpCode) {
        this.erpCode = erpCode;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public String getRepresentative() {
        return representative;
    }

    public void setRepresentative(String representative) {
        this.representative = representative;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public Integer getDataType() {
        return dataType;
    }

    public void setDataType(Integer dataType) {
        this.dataType = dataType;
    }

    public String getUserUpload() {
        return userUpload;
    }

    public void setUserUpload(String userUpload) {
        this.userUpload = userUpload;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getFlowSign() {
        return flowSign;
    }

    public void setFlowSign(Integer flowSign) {
        this.flowSign = flowSign;
    }

    public Integer getAmountB() {
        return amountB;
    }

    public void setAmountB(Integer amountB) {
        this.amountB = amountB;
    }

    public Integer getConfirm() {
        return confirm;
    }

    public void setConfirm(Integer confirm) {
        this.confirm = confirm;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getIsSentMail() {
        return isSentMail;
    }

    public void setIsSentMail(Integer isSentMail) {
        this.isSentMail = isSentMail;
    }
}
