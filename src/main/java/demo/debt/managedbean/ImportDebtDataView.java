package demo.debt.managedbean;

import demo.debt.dto.DebtTempDTO;
import demo.debt.entity.DebtTemp;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.util.List;

@ManagedBean
@ViewScoped
public class ImportDebtDataView {
    private UploadedFile file;
    private List<DebtTempDTO> debtTempList;

    public void uploadFile(FileUploadEvent event){
        file = event.getFile();
        if(file != null){
            showMessageToClient("Upload file " + file.getFileName() + " thành công!");
        }
        else {
            showMessageToClient("Upload file thất bại!");
        }
    }

    public void showDataToGrip() {
        if(file == null){
            showMessageToClient("Không có file!");
        }
        else {
            showMessageToClient(file.getFileName());
        }
    }

    public void showMessageToClient(String message){
        FacesMessage facesMessage = new FacesMessage(message);
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
    }


//    getter & setter
    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public List<DebtTempDTO> getDebtTempList() {
        return debtTempList;
    }

    public void setDebtTempList(List<DebtTempDTO> debtTempList) {
        this.debtTempList = debtTempList;
    }
}
