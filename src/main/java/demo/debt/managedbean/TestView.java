package demo.debt.managedbean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class TestView {
    private String text = "AAAAAAAAAA";

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
